package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/moovweb/gokogiri"
	"github.com/moovweb/gokogiri/xml"
	"html"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

// Data structure for representing a single web comic image and related data.
type Comic struct {
	Title        string
	Link         string
	ImageUrl     string
	ImageComment string
	Date         string
	UnixDate     int64
	PubMsg       string
}

// Sets questionable data member values to preferred defaults.
func (c *Comic) sanitize() {
	if (*c).Title == "." { // Hack: yahoo pipes will not allow blank title.
		(*c).Title = ""
	}
	// Do not allow repition of title in image comment.
	if (*c).ImageComment == (*c).Title {
		(*c).ImageComment = ""
	}
}

// Data structure for multiple comics published by a single site or author.
type ComicSeries struct {
	SeriesTitle string
	SiteUrl     string
	Description string
	Index       int // For the front-end to keep track of Comic to display.
	Comics      []Comic
}

// Sets questionable data member values to preferred defaults.
func (c *ComicSeries) sanitize() {
	// Hacks to accomodate bogus data values from yahoo pipes.
	if (*c).Description == "." || (*c).Description == "Pipes Output" {
		(*c).Description = " "
	}
	// Do not allow repetition of the comic series title.
	for _, comic := range (*c).Comics {
		if comic.Title == (*c).SeriesTitle {
			comic.Title = ""
		}
	}
}

// Data structure for meta data relevant to obtaining a Comic or ComicSeries.
type ComicMetaData struct {
	Url        string
	ImgAttrs   map[string]string
	ImgComment string
	Name       string
	RawRSS     []byte
}

// Requests data from a URL and stores response data.
func (c *ComicMetaData) downloadUrl(wg *sync.WaitGroup) {
	resp, err := http.Get(c.Url)
	if err != nil {
		log.Println("Did not receive HTTP GET response: ", err)
	}
	if resp.StatusCode != 200 {
		log.Printf("Bad HTTP Response %d for %s\n", resp.StatusCode, c.Url)
	}
	defer resp.Body.Close()
	(*c).RawRSS, _ = ioutil.ReadAll(resp.Body)
	(*wg).Done()
}

// Convenience function to obtain child tag content from a Gokogiri XML node.
func getChildTagContent(node xml.Node, tagName string) (content string) {
	nodes, _ := node.Search(tagName)
	if len(nodes) < 1 {
		nodes, _ = node.Search("//" + tagName)
	}
	if len(nodes) > 0 {
		content = nodes[0].Content()
	} else {
		content = ""
	}
	return
}

// Obtains image data from an XML <description> node.
func getImageData(imageNode xml.Node, commentAttrName string) (
	imageUrl string, imageComment string, err error) {
	url := imageNode.Attr("src")
	if strings.Index(url, "plugins") < 0 { // Hack for social network plugin images.
		imageUrl = imageNode.Attr("src")
		imageComment = imageNode.Attr(commentAttrName)
		err = nil
	}
	if imageUrl == "" {
		err = errors.New("No image src in item.")
	}
	return
}

// Calculates, formats, and returns time information about an RSS <pubDate>.
func getDateData(pubDate string) (
	newPubDate string, unixDate int64, pubMsg string, err error) {
	dateTime, err := time.Parse(time.RFC1123Z, pubDate)
	if err != nil {
		dateTime, err = time.Parse(time.RFC1123, pubDate)
	}
	dateTime = dateTime.Local()
	newPubDate = dateTime.Format(time.RFC1123)
	unixDate = dateTime.Unix()
	pubMsg = lastUpdate(unixDate, time.Now().Unix())
	return
}

// Creates an English sentence stating how much time has elapsed 
// between 2 points in Unix time.
func lastUpdate(then, now int64) (lastUpdate string) {
	var hourLength int64 = 60 * 60
	var dayLength int64 = hourLength * 24
	diffTime := now - then
	lastUpdate = "Published "
	if diffTime > dayLength {
		days := diffTime / dayLength
		lastUpdate += fmt.Sprintf("%d day", days)
		if days > 1 {
			lastUpdate += "s"
		}
		lastUpdate += " ago on"
	} else if diffTime > hourLength {
		lastUpdate += fmt.Sprintf("%d hours ago on", diffTime/hourLength)
	} else {
		lastUpdate += "less than 1 hour ago on"
	}
	return
}

// Obtains Comic data from a single RSS <item> node.
func getComic(itemNode xml.Node, commentAttrName string) (c Comic, err error) {
	var imageNodes []xml.Node
	link := getChildTagContent(itemNode, "link")
	title := getChildTagContent(itemNode, "title")
	pubDate := getChildTagContent(itemNode, "pubDate")
	pubDate, unixDate, pubMsg, err := getDateData(pubDate)
	imageNodes, err = itemNode.Search("description//img")
	if err != nil {
		return Comic{}, err
	}
	// A hack because CDATA sequences aren't handled by Gokogiri.
	if len(imageNodes) == 0 {
		descNodes, _ := itemNode.Search("description")
		descNode := descNodes[0]
		descContent := html.UnescapeString(descNode.Content())
		desc, _ := gokogiri.ParseHtml([]byte(descContent))
		imageNodes, _ = desc.Search("//img")
	}
	if len(imageNodes) > 0 { // Ignore RSS items without image tags.
		imageNode := imageNodes[0]
		imageUrl, imageComment, err := getImageData(imageNode, commentAttrName)
		if err != nil {
			return Comic{}, err
		}
		c = Comic{title, link, imageUrl, imageComment, pubDate, unixDate, pubMsg}
		c.sanitize()
	}
	return
}

// Obtains an entire series of Comics from an RSS feed.
func getComicSeries(RSSFeedBody []byte, commentAttrName string) (ComicSeries, error) {
	if commentAttrName == "" {
		commentAttrName = "alt"
	}
	doc, _ := gokogiri.ParseXml(RSSFeedBody)
	defer doc.Free()
	channelNodes, _ := doc.Search("//channel")
	if len(channelNodes) < 1 {
		return ComicSeries{}, errors.New("No channnel node found.")
	}
	// Gokogiri Search results are alwasy wrapped in slices.
	channelNode := channelNodes[0]

	seriesTitle := getChildTagContent(channelNode, "title")
	siteUrl := getChildTagContent(channelNode, "link")
	description := getChildTagContent(channelNode, "description")
	lastBuildDate := getChildTagContent(channelNode, "lastBuildDate")

	itemNodes, _ := channelNode.Search("//item")
	var comics []Comic
	for _, itemNode := range itemNodes {
		comic, err := getComic(itemNode, commentAttrName)
		if err != nil {
			log.Println(err)
			continue
		}
		if comic.UnixDate < 0 { // Hack: Some comics don't have pubDate on items.
			comic.Date, comic.UnixDate, comic.PubMsg, _ = getDateData(lastBuildDate)
		}
		comics = append(comics, comic)
	}

	series := ComicSeries{seriesTitle, siteUrl, description, 0, comics}
	series.sanitize()
	return series, nil
}

// Obtains ComicSeries data for each RSS feed (represented by meta data).
func parseFeeds(metaData []*ComicMetaData) (comics []ComicSeries) {
	for _, md := range metaData {
		var comic ComicSeries
		comic, err := getComicSeries(md.RawRSS, md.ImgComment)
		if md.Name != "" {
			comic.SeriesTitle = md.Name
		}
		if len(comic.Comics) != 0 {
			comics = append(comics, comic)
		} else {
			log.Printf("Error with %s, no actual comics in feed.\n", comic.SiteUrl)
			if err != nil {
				log.Printf("Received error %s for %s", err, comic.SiteUrl)
			}
		}
	}
	return
}

// Helper method for concurrently domnloading RSS feed data from a URL.
func downloadFeeds(config []*ComicMetaData) {
	var wg sync.WaitGroup
	for _, c := range config {
		wg.Add(1)
		go c.downloadUrl(&wg)
	}
	wg.Wait()
}

// Parses a configuration file specifying RSS feeds and 
// constructs meta data to represent them.
func parseConfig(configFileName string) []*ComicMetaData {
	data, _ := ioutil.ReadFile(configFileName)
	var config []ComicMetaData
	json.Unmarshal(data, &config)
	var ptrs []*ComicMetaData
	for i, _ := range config {
		ptrs = append(ptrs, &config[i])
	}
	return ptrs
}

// A basic quicksort algorithm for sorting a ComicSeries by 
// chronological order for the publishing date.
func quickSort(series []ComicSeries) []ComicSeries {
	length := len(series)
	if length < 2 {
		return series
	}
	defer func() {
		if err := recover(); err != nil {
			log.Println("quickSort failed: ", err)
		}
	}()
	pivot := (length / 2) - 1
	var lesser, greater []ComicSeries
	for index, _ := range series {
		if index == pivot {
			continue
		}
		time1 := series[index].Comics[0].UnixDate
		time2 := series[pivot].Comics[0].UnixDate
		if time1 <= time2 {
			lesser = append(lesser, series[index])
		} else {
			greater = append(greater, series[index])
		}
	}
	lesserLen := len(lesser)
	result := make([]ComicSeries, lesserLen+1+len(greater))
	copy(result, quickSort(lesser))
	copy(result[lesserLen:], series[pivot:pivot+1])
	copy(result[lesserLen+1:], quickSort(greater))
	return result
}

// Reverses the sort order of a ComicSeries.
func reverse(series []ComicSeries) []ComicSeries {
	j := len(series) - 1
	result := make([]ComicSeries, len(series))
	for i, _ := range series {
		result[j-i] = series[i]
	}
	return result
}

// Functions for use in the HTML template.
func incr(n int) string { return fmt.Sprintf("%d", n+1) }
func decr(n int) string { return fmt.Sprintf("%d", n-1) }

func main() {
	// Sanity check and construct template first.
	tmplText, _ := ioutil.ReadFile("template.html")
	tmplString := string(tmplText)
	funcMap := template.FuncMap{"incr": incr, "decr": decr}
	tmpl, _ := template.New("comics").Funcs(funcMap).Parse(tmplString)

	// Set up configuration.
	metaData := parseConfig("config.json")

	// Download and parse feeds.
	downloadFeeds(metaData)
	comics := parseFeeds(metaData)

	// Sort and output results.
	outputFile, _ := os.Create("index.html")
	comics = reverse(quickSort(comics))
	tmpl.Execute(outputFile, comics)

	// Output data into JSON file for use by the front end.
	jsonData, _ := json.Marshal(comics)
	ioutil.WriteFile("comics.json", jsonData, 0644)

	// Serve the front-end and generated data.
	panic(http.ListenAndServe(":8080", http.FileServer(http.Dir("."))))
}
